﻿using ModuleA.Server.Database.Models;
using ModuleA.Contracts.Models;
using Newtonsoft.Json;


namespace ModuleA.Server.Integration;

public static class FolderIntegration
{
    public static List<FolderItem> TransformNestedSetToDtosHierarchy(List<NestedSetNode> nestedSetNodes)
    {
        return GenerateChildren(0, nestedSetNodes[0].Right, nestedSetNodes.GetRange(1, nestedSetNodes.Count - 1));
    }

    private static List<FolderItem> GenerateChildren(int currentLeft, int currentRight, List<NestedSetNode> nestedSetNodes)
    {
        if (nestedSetNodes.Count == 0)
        {
            return new List<FolderItem>();
        }

        NestedSetNode nestedSetNode = nestedSetNodes[0];
        List<NestedSetNode> tail = nestedSetNodes.GetRange(1, nestedSetNodes.Count - 1);

        if (nestedSetNode.Left > currentRight)
        {
            return new List<FolderItem>();
        }
        else if (nestedSetNode.Left > currentLeft)
        {
            List<FolderItem> children = GenerateChildren(nestedSetNode.Left, nestedSetNode.Right, tail);
            List<FolderItem> siblingChildren = GenerateChildren(nestedSetNode.Right, currentRight, tail);
            return new List<FolderItem> { new FolderItem() { Id = nestedSetNode.Id, Name = nestedSetNode.Name, Children = children } }.Concat(siblingChildren).ToList();
        }
        else
        {
            return GenerateChildren(currentLeft, currentRight, tail);
        }
    }
}
