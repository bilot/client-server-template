﻿using log4net;
using MvvmGen.Events;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Regions;
using CommonServiceLocator;
using ModuleA.Client.Services;
using ModuleA.Client.ViewModels;
using ModuleA.Client.Views;

namespace ModuleA.Client.Startup
{
    [Module(ModuleName = "ModuleA")]
    public class ModuleA : IModule
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ModuleA));

        public void OnInitialized(IContainerProvider containerProvider)
        {
            Logger.Info("OnInitialized");

            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(FolderView));

            var eventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();
            eventAggregator.RegisterSubscriber(this);
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //Register Instances
            containerRegistry.RegisterInstance<IAPIService>(new APIService());

            //Register Types
            containerRegistry.RegisterSingleton<IAPIService, APIService>();
            //containerRegistry.RegisterSingleton<IEventAggregator, EventAggregator>();

            ViewModelLocationProvider.Register<FolderView, FolderViewModel>();
        }

    }
}