# Effektive Projektorganisation und Strukturierung in der Softwareentwicklung

Die Organisation und Strukturierung von Softwareprojekten sind entscheidende Faktoren für deren Erfolg. Insbesondere in komplexen Anwendungen wie ClientServerTemplateen ist eine klare Strukturierung unerlässlich, um eine effiziente Entwicklung, Wartung und Skalierung zu ermöglichen. In diesem Artikel werden wir am Beispiel einer ClientServerTemplate die Projektorganisation und -struktur beleuchten.

## Beispielanwendung: Client-Server-Anwendung zur Verwaltung einer hierarchischen Ordnerstruktur

- [ANFORDERUNGEN](REQUIREMENTS.md)
- [TECHNIKKONZEPT](TECHSTECK.md)
- [DATENMODELL](DATAMODEL.md)

## Projektaufbau

Um eine klare Trennung von Verantwortlichkeiten und eine einfache Skalierbarkeit zu gewährleisten, strukturieren wir unser Projekt in verschiedene Module:

### Client-Shell

Die Client-Shell besteht nur aus einem Modul:

- **Shell**: Dieses Modul ist für die Erzeugung der Benutzeroberfläche des Clients verantwortlich. Es dient als zentrales Steuerelement und lädt dynamisch die erforderlichen Module, um die Funktionalität des Clients bereitzustellen.

Die Client-Shell ist das Hauptmodul der Anwendung und fungiert als Einstiegspunkt für den Benutzer. Sie bietet die Grundstruktur und Navigation der Anwendung und orchestriert die Interaktionen mit den verschiedenen Modulen.

### Client-Modul

Das Client-Modul besteht aus mehreren Untermodule:

- **ModuleA.Client.Startup**: Enthält den Startpunkt der Client-Anwendung.
- **ModuleA.Client.Events**: Implementiert Ereignisse für den Prism EventAggregator.
- **ModuleA.Client.Models**: Hier werden Client-spezifische Modelle definiert.
- **ModuleA.Client.Services**: Hier werden Client-spezifische REST-Schnittstellen implementiert. 
- **ModuleA.Client.Views**: Enthält die grafischen Benutzeroberflächen der Anwendung.
- **ModuleA.Client.ViewModels**: Hier werden View-Modelle bereitgestellt.

### Server-Modul

Auf der Serverseite gliedert sich das Modul in folgende Bereiche:

- **ModuleA.Server.Controllers**: Definiert die Controller für die Serveranwendung.
- **ModuleA.Server.Integration**: Beinhaltet Integrationskomponenten für externe Systeme.
- **ModuleA.Server.Database**: Verwaltet Datenbankzugriffe und -operationen.
- **ModuleA.Server.Webservice**: Implementiert Webdienste und Schnittstellen für die Client-Kommunikation.

### Client-Server-Modul

Gemeinsame Module, die sowohl auf Client- als auch auf Serverseite verwendet werden:

- **ModuleA.Contracts**: Das Modul beinhaltet Datenübertragungsobjekte, auch bekannt als DTOs (Data Transfer Objects).


### Test-Modul

Um die Qualität und Stabilität unseres Codes sicherzustellen, führen wir umfangreiche Tests durch. Diese sind in einem separaten Test-Modul organisiert:

  - **ModuleA.Server.Controllers.Tests**: Testfälle für die Controller.
  - **ModuleA.Server.Database.Tests**: Testfälle für die Datenbankzugriffe.
  - **ModuleA.Server.Integration.Tests**: Integrationstests für externe Systeme.

## Dynamische Modulladung

Um Flexibilität und Erweiterbarkeit zu gewährleisten, setzen wir auf eine dynamische Modulladung. Das bedeutet, dass die Module zur Laufzeit geladen werden können, ohne dass die Hauptanwendung davon Kenntnis haben muss. Dadurch können neue Funktionen und Module einfach hinzugefügt werden, ohne dass bestehender Code angepasst werden muss.

## Schlussfolgerung

Die effektive Organisation und Strukturierung eines Softwareprojekts sind von entscheidender Bedeutung für dessen Erfolg. Durch eine klare Trennung von Verantwortlichkeiten, eine modulare Architektur und den Einsatz von bewährten Technologien und Frameworks können Entwickler effizient arbeiten, die Wartbarkeit verbessern und die Skalierbarkeit der Anwendung gewährleisten. Unser Beispielprojekt einer ClientServerTemplate zeigt, wie diese Prinzipien in der Praxis umgesetzt werden können.

## ToDo

### Verbesserungn in Client Modul verwaltug

Um die Module dynamisch zu laden, verwenden wir einen DirectoryModuleCatalog von Prism. 
Dieser sucht nach Modulen in einem angegebenen Verzeichnis. 
In unserem Fall ist das Verzeichnis "Modules". 

Die bisherige Struktur des "Modules"-Verzeichnisses sieht folgendermaßen aus:

```text
Modules
|___ModuleA.Client.Events.dll
|___ModuleA.Client.Models.dll
|___ModuleA.Client.Services.dll
|___ModuleA.Client.Startup.dll
|___ModuleA.Client.ViewModels.dll
|___ModuleA.Client.Views.dll
|___ModuleA.Contracts.dll
|___ModuleB.Client.Events.dll
|___ModuleB.Client.Models.dll
|___ModuleB.Client.Services.dll
|___ModuleB.Client.Startup.dll
|___ModuleB.Client.ViewModels.dll
|___ModuleB.Client.Views.dll
|___ModuleB.Contracts.dll
```text


Um eine klarere Struktur und Organisation zu gewährleisten, haben wir beschlossen, jedes Modul in einem eigenen Verzeichnis unterhalb des "Modules"-Verzeichnisses abzulegen. Das bedeutet, dass jedes Modul seinen eigenen Ordner erhält, benannt nach dem Modulnamen. 

Die neue Struktur des "Modules"-Verzeichnisses sieht nun wie folgt aus:

```text
Modules
|___ModuleA
|   |___ModuleA.Client.Events.dll
|   |___ModuleA.Client.Models.dll
|   |___ModuleA.Client.Services.dll
|   |___ModuleA.Client.Startup.dll
|   |___ModuleA.Client.ViewModels.dll
|   |___ModuleA.Client.Views.dll
|   |___ModuleA.Contracts.dll
|___ModuleB
    |___ModuleB.Client.Events.dll
    |___ModuleB.Client.Models.dll
    |___ModuleB.Client.Services.dll
    |___ModuleB.Client.Startup.dll
    |___ModuleB.Client.ViewModels.dll
    |___ModuleB.Client.Views.dll
    |___ModuleB.Contracts.dll
```

Diese neue Struktur ermöglicht eine bessere Organisation der Module und erleichtert die Wartun

### Zustand der Client-Module speichern und wiederherstellen

Um den Zustand der Client-Module zu speichern und beim Start wiederherzustellen, gibt es verschiedene Strategien. Grundsätzlich können zwei unterschiedliche Varianten implementiert werden:

1. **Speicherung des Zustands beim Beenden des Client-Programms:**
  In dieser Variante wird der Zustand der Module beim Beenden des Programms gespeichert und beim nächsten Start des Programms wiederhergestellt. 
	
    Bei realisicrung disen variante soll kommunikation zwichen Client-Shell (Hauptmodul) und verschiedenen Modulen zum zeitpunkt Beenden sicher funktioniert dar in Prism keine implezurte funktion dazu zu verfügung steit so wie "public void OnInitialized(IContainerProvider containerProvider)" bei Start.

2. **Speicherung des Zustands bei Änderung des Zustands:**
    Bei dieser Variante wird der Zustand der Module jedes Mal gespeichert, wenn sich der Zustand ändert. Dies kann beispielsweise beim Schließen oder Öffnen von Verzeichnissen oder beim Ändern der Spaltenbreite eines Grids erfolgen. Durch diese kontinuierliche Speicherung wird sichergestellt, dass der aktuelle Zustand immer gespeichert ist, jedoch kann dies zu einer erhöhten Anzahl von Speicherzugriffen führen.

Je nach Anforderungen und Benutzererwartungen kann eine der beiden Strategien ausgewählt und implementiert werden. Dabei sollte darauf geachtet werden, dass die Speicherung und Wiederherstellung des Zustands effizient und benutzerfreundlich erfolgt.



