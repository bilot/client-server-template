Programmiersprache C#

Der Client verwendet folgende Technologien:

1. **WPF (Windows Presentation Foundation)**:
   Windows Presentation Foundation ist ein Framework von Microsoft, das für die Erstellung von Desktop-Anwendungen unter Windows verwendet wird. Es bietet eine umfassende Set von Bibliotheken, um eine moderne und ansprechende Benutzeroberfläche zu erstellen, die sowohl visuell ansprechend als auch funktional ist. WPF verwendet XAML (eXtensible Application Markup Language) für das Design von Benutzeroberflächen und unterstützt die Trennung von Design und Logik.

2. **MvvmGen**:
   MvvmGen ist ein Toolkit zur Implementierung des MVVM (Model-View-ViewModel)-Musters in WPF-Anwendungen. Es automatisiert viele der wiederkehrenden Aufgaben im Zusammenhang mit MVVM, wie die Erstellung von ViewModel-Klassen, die Implementierung von INotifyPropertyChanged und ICommand-Schnittstellen sowie die Generierung von Datenbindungscodes. Dies ermöglicht eine schnellere Entwicklung von WPF-Anwendungen durch Reduzierung des Boilerplate-Codes.

3. **Prism**:
   Prism ist ein Framework, das sich auf die Entwicklung von modularen und erweiterbaren WPF- und Xamarin-Anwendungen konzentriert. Es bietet eine Reihe von Funktionen, die die Entwicklung großer Anwendungen erleichtern, darunter Unterstützung für die Implementierung des MVVM-Musters, die Verwaltung von Modulen, die Navigation zwischen Ansichten und die Unterstützung für die Integration von DI (Dependency Injection)-Containern.

4. **Refit**:
   Refit ist eine Bibliothek, die die Erstellung von RESTful-API-Clients in .NET-Anwendungen erleichtert. Sie ermöglicht die Definition von Schnittstellen, die den Strukturen von RESTful-API-Endpunkten entsprechen, und generiert automatisch Implementierungen dieser Schnittstellen zur Kommunikation mit dem Server. Dies reduziert den manuellen Aufwand bei der Erstellung von API-Clients erheblich und verbessert die Wartbarkeit des Codes.

5. **log4net**:
   log4net ist ein Framework für das Logging in .NET-Anwendungen. Es bietet eine flexible und leistungsfähige Logging-Lösung, die es Entwicklern ermöglicht, Protokolle in verschiedenen Formaten und auf verschiedenen Ausgabezielen zu speichern. log4net unterstützt Funktionen wie Protokollierungslevel, Log-Rotation, Nachrichtenfilterung und die Konfiguration über XML-Dateien oder programmgesteuerte Konfiguration. Es ist eine beliebte Wahl für das Logging in .NET-Anwendungen aufgrund seiner Flexibilität und Anpassungsfähigkeit.


Die Serverseite nutzt die Technologien:

1. **Microsoft.AspNetCore**:
   Microsoft.AspNetCore ist ein Framework von Microsoft für die Entwicklung von Webanwendungen und APIs in der Programmiersprache C#. Es ist Teil des .NET Core-Ökosystems und bietet eine Reihe von Funktionen und Diensten, die für die Entwicklung moderner und leistungsfähiger Webanwendungen erforderlich sind. Dazu gehören Routing, Middleware zur Verarbeitung von HTTP-Anforderungen, Unterstützung für Dependency Injection, Authentifizierung, Autorisierung, Logging und vieles mehr. Microsoft.AspNetCore ermöglicht die plattformübergreifende Entwicklung von Webanwendungen und ist die Grundlage für viele ASP.NET Core-Anwendungen.

2. **NHibernate**:
   NHibernate ist ein objektrelationales Mapping-Framework (ORM) für die .NET-Plattform. Es bietet eine Möglichkeit, Objekte in einer Anwendung direkt mit Datenbanktabellen zu verknüpfen, ohne SQL-Code schreiben zu müssen. NHibernate ermöglicht eine objektorientierte Herangehensweise an die Datenbankinteraktion und abstrahiert viele der komplexen Aufgaben im Zusammenhang mit Datenzugriff und Datenbankverwaltung. Es unterstützt Features wie Lazy Loading, Transaktionen, Caching und die Verwendung von LINQ-Abfragen zur Datenabfrage. NHibernate ist eine beliebte Wahl für die Datenzugriffsschicht in .NET-Anwendungen.

3. **log4net**:
   log4net ist ein Framework für das Logging in .NET-Anwendungen. Es bietet eine flexible und leistungsfähige Logging-Lösung, die es Entwicklern ermöglicht, Protokolle in verschiedenen Formaten und auf verschiedenen Ausgabezielen zu speichern. log4net unterstützt Funktionen wie Protokollierungslevel, Log-Rotation, Nachrichtenfilterung und die Konfiguration über XML-Dateien oder programmgesteuerte Konfiguration. Es ist eine beliebte Wahl für das Logging in .NET-Anwendungen aufgrund seiner Flexibilität und Anpassungsfähigkeit.


Für das Testen kommen zum Einsatz:
1. **NUnit**:
   NUnit ist ein Framework für das unit testing in .NET-Anwendungen. Es bietet eine einfache und elegante Syntax für das Schreiben und Ausführen von Tests in C#, VB.NET oder anderen .NET-Sprachen. Mit NUnit können Entwickler Testfälle erstellen, um sicherzustellen, dass ihr Code wie erwartet funktioniert und dass zukünftige Änderungen den vorhandenen Code nicht brechen. NUnit unterstützt verschiedene Funktionen wie Parametertests, Testdatenbanken, paralleles Testen und die Integration mit Continuous-Integration-Tools wie Jenkins oder TeamCity.

2. **Shouldly**:
   Shouldly ist eine Erweiterungsbibliothek für NUnit (oder andere unit testing Frameworks), die eine erweiterte Syntax für die Überprüfung von Testergebnissen bietet. Im Vergleich zu den standardmäßigen Assert-Funktionen in NUnit bietet Shouldly eine leserlichere und ausdrucksstärkere Syntax, die das Schreiben und Verstehen von Tests erleichtert. Zum Beispiel ermöglicht Shouldly, Testergebnisse mit Ausdrücken wie `result.ShouldNotBeNull()` oder `result.ShouldBeGreaterThan(5)` zu überprüfen, wodurch die Absicht des Tests klarer wird und die Lesbarkeit des Codes verbessert wird. Shouldly ist besonders nützlich für Entwickler, die Wert auf sauberen und gut lesbaren Testcode legen.
