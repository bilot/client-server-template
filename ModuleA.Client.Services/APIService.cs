﻿using System.Configuration;
using Refit;
using ModuleA.Contracts.Models;
using log4net;
using System.Xml.Linq;

namespace ModuleA.Client.Services;

public class APIService : IAPIService
{
    private static readonly ILog Logger = LogManager.GetLogger(typeof(APIService));
    IModuleAAPI ApiResponse = RestService.For<IModuleAAPI>(ConfigurationManager.AppSettings["ModulAApiUrl"]);

    async public Task<List<FolderItem>> GetAllFolder()
    {
        try
        {
            return await ApiResponse.GetAllFolder();
        }
        catch (ApiException ex)
        {
            Logger.Error(ex);
            return new List<FolderItem>();
        }
    }

    async public Task AddFolder(int parentId)
    {
        try
        {
            await ApiResponse.AddFolder(parentId);
        }
        catch (ApiException ex)
        {
            Logger.Error(ex);
        }
    }

    async public Task RenameFolder(int id, string name)
    {
        try
        {
            await ApiResponse.RenameFolder(id, name);
        }
        catch (ApiException ex)
        {
            Logger.Error(ex);
        }
    }

    async public Task DeleteFolder(int id)
    {
        try
        {
            await ApiResponse.DeleteFolder(id);
        }
        catch (ApiException ex)
        {
            Logger.Error(ex);
        }
    }

    async public Task MoveFolder(int id, int parentId)
    {
        try
        {
            await ApiResponse.MoveFolder(id, parentId);
        }
        catch (ApiException ex)
        {
            Logger.Error(ex);
        }       
    }
}
