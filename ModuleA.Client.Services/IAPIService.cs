﻿using ModuleA.Contracts.Models;
using Refit;

namespace ModuleA.Client.Services;

[Headers("Content-Type: application/json")]
public interface IAPIService
{
    //Folder endpoint tasks
    Task<List<FolderItem>> GetAllFolder();
    Task AddFolder(int parentId);
    Task RenameFolder(int id, string name);
    Task DeleteFolder(int id);
    Task MoveFolder(int id, int parentId);
}
