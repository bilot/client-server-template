﻿using Refit;
using ModuleA.Contracts.Models;

namespace ModuleA.Client.Services;

public interface IModuleAAPI
{
    [Get("/api/folder")]
    Task<List<FolderItem>> GetAllFolder();

    [Get("/api/folder/add/{parentId}")]
    Task<FolderItem> AddFolder(int parentId);

    [Get("/api/folder/rename/{id}/{name}")]
    Task RenameFolder(int id, string name);

    [Delete("/api/folder/{id}")]
    Task DeleteFolder(int id);

    [Get("/api/folder/move/{id}/{parentId}")]
    Task MoveFolder(int id, int parentId);
}
