﻿using CommonServiceLocator;
using log4net;
using log4net.Config;
using MvvmGen.Events;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Unity;
using Shell.Views;
using System.Windows;

namespace Shell;

public partial class App : PrismApplication
{
    private static readonly ILog Logger = LogManager.GetLogger(typeof(App));

    protected override void OnStartup(StartupEventArgs e)
    {
        XmlConfigurator.Configure();
        LogBasicInfo();

        //ProcessController.CheckSingleton(ProcessName, (IntPtr)Settings.Default.WindowHandle);
        //ConfigureApplicationEventHandlers();
        base.OnStartup(e);
    }

    protected override Window CreateShell()
    {
        // Hier wird normalerweise ein Window zurükgegeben. Da wir aber ein RadTabbedWindow nutzen, gibt es den Workaround in OnInitailized
        return null;
    }

    protected override void OnInitialized()
    {
        MainWindow mainWindow = Container.Resolve<MainWindow>();
        mainWindow.Show();

        base.OnInitialized();
    }

    protected override void RegisterTypes(IContainerRegistry containerRegistry)
    {
        containerRegistry.RegisterSingleton<IEventAggregator, EventAggregator>();
    }

    protected override IModuleCatalog CreateModuleCatalog()
    {
        return new DirectoryModuleCatalog() { ModulePath = @".\Modules" };
    }


    private static void LogBasicInfo()
    {
        Logger.Info(
            //$"Accelerider for Windows: {AcceleriderConsts.Version};{Environment.NewLine}" +
            //$"OS: {SystemInfo.Caption} ({SystemInfo.Version}) {SystemInfo.OSArchitecture};{Environment.NewLine}" +
            //$".NET: {SystemInfo.DotNetFrameworkVersion};{Environment.NewLine}" +
            $"CLR: {Environment.Version};{Environment.NewLine}" +
            //$"Processor: {SystemInfo.CPUName};{Environment.NewLine}" +
            //$"RAM: {(DisplayDataSize)(SystemInfo.TotalVisibleMemorySize * 1024)};
            "");
    }

}
