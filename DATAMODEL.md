## Client-seitiges Model (MVVM-generiert):

```csharp
using MvvmGen;
using System.Collections.ObjectModel;

namespace ModuleA.Client.Models
{
    [ViewModelGenerateInterface]
    [ViewModel]
    public partial class FolderItem
    {
        [Property] private int _id;
        [Property] private string _name;
        [Property] private bool _isExpanded;
        [Property] private bool _isSelected;
        [Property] private bool _isEdited;
        [Property] private ObservableCollection<FolderItem> _children;
    }
}
```
Dieses Modell wird im Client verwendet und ist speziell für die Darstellung in einer WPF-TreeView angepasst. Es enthält Eigenschaften wie die ID, den Namen des Ordners, ob er erweitert oder ausgewählt ist, ob er bearbeitet wird, und eine Liste von Unterelementen.

## Data Transfer Object für die Server-Client-Kommunikation:
```csharp
namespace ModuleA.Contracts.Models
{
    public class FolderItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<FolderItem> Children { get; set; } = new List<FolderItem>();
    }
}
```
Dieses DTO wird für die Kommunikation zwischen Server und Client verwendet. Es spiegelt die hierarchische Struktur von Ordnerelementen wider und enthält ID, Namen und eine Liste von Unterelementen.

## Server-seitiges Datenbankmodell (Nested Set):



```csharp
namespace ModuleA.Server.Database.Models
{
    public partial class NestedSetNode
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; } = null!;
        public virtual int Left { get; set; }
        public virtual int Right { get; set; }
        public virtual int Level { get; set; }
    }
}
```
Dieses Modell wird verwendet, um die hierarchische Ordnerstruktur in der Datenbank zu speichern. Es verwendet das Nested-Set-Modell, um die Position und Beziehungen der Ordner in der Hierarchie darzustellen.

### Nested Set-Modell

Das Nested Set-Modell ist eine Methode zur Speicherung hierarchischer Daten in relationalen Datenbanken. Es verwendet ein spezielles Schema, bei dem jedes Element im Baum durch eine "linke" und "rechte" Grenze repräsentiert wird, um die Hierarchie darzustellen. Dies ermöglicht effiziente Abfragen wie das Abrufen von Unterstruktur oder das Ermitteln der Tiefe eines Elements. Im Vergleich zu anderen Modellen wie Adjazenzlisten bietet Nested Sets keine direkte Repräsentation von Eltern-Kind-Beziehungen, was die Aktualisierung und Verwaltung der Hierarchie komplex machen kann. Dennoch wird es in bestimmten Szenarien verwendet, in denen hierarchische Daten effizient abgefragt werden müssen.



Diese verschiedenen Modelle erfüllen unterschiedliche Zwecke innerhalb Ihrer Anwendung und dienen dazu, Daten in verschiedenen Teilen der Anwendung zu repräsentieren, zu übertragen und zu speichern.


