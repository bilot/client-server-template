﻿using log4net.Config;
using log4net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace ModuleA.Server.Webservice;

class Program
{
    private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));

    static int Main(string[] args)
    {
        XmlConfigurator.Configure();

        try
        {
            CreateHostBuilder(args).Build().Run();
            return 0;
        }
        catch (Exception exception)
        {
            HandleException(exception);
            return 1;
        }
    }

    static void HandleException(Exception exception)
    {
        Console.WriteLine("Ein Fehler ist aufgetreten. " + exception.Message);
        Logger.Error("Ein Fehler ist aufgetreten. ", exception);
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(builder =>
                {
                    builder.ConfigureLogging((hostingContext, logging) =>
                    {
                        //https://abarrenechea.net/articles/add-logging-to-aconsole-app

                        logging.AddLog4Net("ModuleA.Server.Webservice.dll.config");

                        //logging.SetMinimumLevel(LogLevel.Debug);
                    });

                    builder.UseUrls("http://localhost:5003");
                    //builder.UseUrls("http://localhost:5003", "https://localhost:5004");
                    builder.UseStartup<Startup>();
                });

}

//private Configuration Configuration;
//private ISessionFactory SessionFactory;
