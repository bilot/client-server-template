using ModuleA.Server.Database.Models;
using ModuleA.Server.Database.Repository;
using NHibernate.Cfg;
using NHibernate;
using System.Xml.Linq;
using NHibernate.Criterion;

namespace ModuleA.Server.Database.Tests.Repository;

[TestFixture]
public class NestedSetRepositoryTests
{
    private static ISessionFactory _sessionFactory;

    private NestedSetRepository _repository;

    [SetUp]
    public void SetUp()
    {
        var cfg = new Configuration().Configure("ModuleA.Server.Database.Tests.dll.config");

        var schemaCreate = new NHibernate.Tool.hbm2ddl.SchemaExport(cfg);
        schemaCreate.Create(false, true);

        _sessionFactory = cfg.BuildSessionFactory();
        _repository = new NestedSetRepository(_sessionFactory.OpenSession());
    }

    [TearDown]
    public void Cleanup()
    {
        _sessionFactory.Close();
        _repository.Dispose();
    }


    [Test]
    public void CreateRoot()
    {
        var expected = new NestedSetNode() { Id = 1, Name = "Root", Left = 0, Right = 1, Level = 0 };
        var result = _repository.CreateRoot(new NestedSetNode() { Name = "Root" });
        result.ShouldBeEquivalentTo(expected);

        result = _repository.GetRootNode();
        result.ShouldBeEquivalentTo(expected);
    }

    [Test]
    public void InsertAsLastChildOf()
    {
        var expected = new List<NestedSetNode>()
        {
            new NestedSetNode() { Id = 1, Name = "Root", Left = 0, Right = 21, Level = 0 },
            new NestedSetNode() { Id = 2, Name = "Ordner 1", Left = 1, Right = 14, Level = 1 },
            new NestedSetNode() { Id = 3, Name = "Ordner 1.1", Left = 2, Right = 3, Level = 2 },
            new NestedSetNode() { Id = 4, Name = "Ordner 1.2", Left = 4, Right = 11, Level = 2 },
            new NestedSetNode() { Id = 5, Name = "Ordner 1.2.1", Left = 5, Right = 6, Level = 3 },
            new NestedSetNode() { Id = 6, Name = "Ordner 1.2.2", Left = 7, Right = 8, Level = 3 },
            new NestedSetNode() { Id = 7, Name = "Ordner 1.2.3", Left = 9, Right = 10, Level = 3 },
            new NestedSetNode() { Id = 8, Name = "Ordner 1.3", Left = 12, Right = 13, Level = 2 },
            new NestedSetNode() { Id = 9, Name = "Ordner 2", Left = 15, Right = 20, Level = 1 },
            new NestedSetNode() { Id = 10, Name = "Ordner 2.1", Left = 16, Right = 17, Level = 2 },
            new NestedSetNode() { Id = 11, Name = "Ordner 2.2", Left = 18, Right = 19, Level = 2 },
        };

        Database.CreateInitialData(_repository);
        
        var result = _repository.GetAll();
        result.ShouldBeEquivalentTo(expected);
    }

    [Test]
    public void MoveToLeft()
    {
        var expected = new List<NestedSetNode>()
        {
            new NestedSetNode() { Id = 1, Name = "Root", Left = 0, Right = 21, Level = 0 },
            new NestedSetNode() { Id = 2, Name = "Ordner 1", Left = 1, Right = 20, Level = 1 },
            new NestedSetNode() { Id = 3, Name = "Ordner 1.1", Left = 2, Right = 3, Level = 2 },
            new NestedSetNode() { Id = 4, Name = "Ordner 1.2", Left = 4, Right = 11, Level = 2 },
            new NestedSetNode() { Id = 5, Name = "Ordner 1.2.1", Left = 5, Right = 6, Level = 3 },
            new NestedSetNode() { Id = 6, Name = "Ordner 1.2.2", Left = 7, Right = 8, Level = 3 },
            new NestedSetNode() { Id = 7, Name = "Ordner 1.2.3", Left = 9, Right = 10, Level = 3 },
            new NestedSetNode() { Id = 8, Name = "Ordner 1.3", Left = 12, Right = 13, Level = 2 },
            new NestedSetNode() { Id = 9, Name = "Ordner 2", Left = 14, Right = 19, Level = 2 },
            new NestedSetNode() { Id = 10, Name = "Ordner 2.1", Left = 15, Right = 16, Level = 3 },
            new NestedSetNode() { Id = 11, Name = "Ordner 2.2", Left = 17, Right = 18, Level = 3 },
        };

        Database.CreateInitialData(_repository);

        var node = _repository.GetById(9);
        var patentNode = _repository.GetById(2);
        _repository.MoveTo(node, patentNode.Right, patentNode.Left + 1);

        var result = _repository.GetAll();
        result.ShouldBeEquivalentTo(expected);
    }

    [Test]
    public void MoveToRight()
    {
        var expected = new List<NestedSetNode>()
        {
            new NestedSetNode() { Id = 1, Name = "Root", Left = 0, Right = 21, Level = 0 },
            new NestedSetNode() { Id = 2, Name = "Ordner 1", Left = 1, Right = 6, Level = 1 },
            new NestedSetNode() { Id = 3, Name = "Ordner 1.1", Left = 2, Right = 3, Level = 2 },
            new NestedSetNode() { Id = 8, Name = "Ordner 1.3", Left = 4, Right = 5, Level = 2 },
            new NestedSetNode() { Id = 9, Name = "Ordner 2", Left = 7, Right = 20, Level = 1 },
            new NestedSetNode() { Id = 10, Name = "Ordner 2.1", Left = 8, Right = 17, Level = 2 },
            new NestedSetNode() { Id = 4, Name = "Ordner 1.2", Left = 9, Right = 16, Level = 3 },
            new NestedSetNode() { Id = 5, Name = "Ordner 1.2.1", Left = 10, Right = 11, Level = 4 },
            new NestedSetNode() { Id = 6, Name = "Ordner 1.2.2", Left = 12, Right = 13, Level = 4 },
            new NestedSetNode() { Id = 7, Name = "Ordner 1.2.3", Left = 14, Right = 15, Level = 4 },
            new NestedSetNode() { Id = 11, Name = "Ordner 2.2", Left = 18, Right = 19, Level = 2 },
        };

        Database.CreateInitialData(_repository);

        var node = _repository.GetById(4);
        var patentNode = _repository.GetById(10);
        _repository.MoveTo(node, patentNode.Right, patentNode.Level + 1);

        var result = _repository.GetAll();
        result.ShouldBeEquivalentTo(expected);
    }

}
