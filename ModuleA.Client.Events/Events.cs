﻿namespace ModuleA.Client.Events;

public record SelectFolderEvent(int parentId);
public record SetFocusToTree();
