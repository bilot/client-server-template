﻿namespace ModuleA.Contracts.Models;

public class FolderItem
{
    public int Id { get; set; }
    public string Name { get; set; }

    public virtual ICollection<FolderItem> Children { get; set; } = new List<FolderItem>();
}
