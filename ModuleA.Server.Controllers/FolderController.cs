﻿using Microsoft.AspNetCore.Mvc;
using log4net;
using ModuleA.Contracts.Models;
using ModuleA.Server.Database.Repository;
using ModuleA.Server.Database.Models;
using ModuleA.Server.Integration;

namespace ModuleA.Server.Controllers;


[Route("api/[controller]")]
[ApiController]
public class FolderController : ControllerBase
{
    private static readonly ILog Logger = LogManager.GetLogger(typeof(FolderController));
    private readonly NestedSetRepository _repository;


    public FolderController()
    {
        _repository = new NestedSetRepository();
    }

    public FolderController(NestedSetRepository repository)
    {
        _repository = repository;
    }

    [HttpGet("")]
    public ActionResult<IEnumerable<FolderItem>> GetAll()
    {
        try
        {
            var allNestedSets = _repository.GetAll().OrderBy(x => x.Left).ToList();
            var allFolderItems = FolderIntegration.TransformNestedSetToDtosHierarchy(allNestedSets);
            return Ok(allFolderItems);
        }
        catch (Exception exception)
        {
            Logger.Error(exception);

            return Problem();
        }
    }


    [HttpGet("add/{parentId}")]
    public ActionResult<FolderItem> AddFolder(int parentId)
    {
        try
        {
            var parent = _repository.GetById(parentId);
            if (parent == null)
            {
                return NotFound();
            }

            _repository.BeginTransaction();
            var name = _repository.GetUniqueName(parent, "Neuer Ordner");
            var node = _repository.InsertAsLastChildOf(parent, new NestedSetNode() { Name = name });
            var folderItem = new FolderItem() { Id = node.Id, Name = node.Name };

            _repository.CommitTransaction();

            return Ok(folderItem);
        }
        catch (Exception exception)
        {
            _repository.RollbackTransaction();
            Logger.Error(exception);

            return Problem();
        }
    }

    [HttpGet("rename/{id}/{name}")]
    public ActionResult RenameFolder(int id, string name)
    {
        try
        {
            var node = _repository.GetById(id);
            if (node == null)
            {
                return NotFound();
            }
            node.Name = name;

            _repository.BeginTransaction();
            _repository.Update(node);
            _repository.CommitTransaction();

            return Ok();
        }
        catch (Exception exception)
        {
            _repository.RollbackTransaction();
            Logger.Error(exception);

            return Problem();
        }
    }

    [HttpDelete("{id}")]
    public ActionResult DeleteFolder(int id)
    {
        try
        {
            var node = _repository.GetById(id);
            if (node == null)
            {
                return NotFound();
            }

            if (node.Level == 0)
            {
                return BadRequest();
            }

            _repository.BeginTransaction();
            _repository.Delete(node);
            _repository.CommitTransaction();

            return Ok();
        }
        catch (Exception exception)
        {
            _repository.RollbackTransaction();
            Logger.Error(exception);

            return Problem();
        }
    }

    [HttpGet("move/{id}/{parentId}")]
    public ActionResult MoveFolder(int id, int parentId)
    {
        try
        {
            var node = _repository.GetById(id);
            if (node == null)
            {
                return NotFound();
            }

            var parentNode = _repository.GetById(parentId);
            if (parentNode == null)
            {
                return NotFound();
            }

            if( node.Left <= parentNode.Left && node.Right >= parentNode.Right) 
            {
                return BadRequest();
            }

            _repository.BeginTransaction();
            // Move to last child of parent node
            _repository.MoveTo(node, parentNode.Right, parentNode.Level + 1);
            _repository.CommitTransaction();

            return Ok();
        }
        catch (Exception exception)
        {
            _repository.RollbackTransaction();
            Logger.Error(exception);

            return Problem();
        }
    }

}