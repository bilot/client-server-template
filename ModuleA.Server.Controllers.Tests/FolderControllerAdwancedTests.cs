using Microsoft.AspNetCore.Mvc;
using NHibernate.Cfg;
using NHibernate;
using ModuleA.Server.Database.Models;
using ModuleA.Server.Database.Repository;
using ModuleA.Contracts.Models;
using ModuleA.Server.Integration;
using Newtonsoft.Json;

namespace ModuleA.Server.Controllers.Tests;

[TestFixture]
public class FolderControllerAdwancedTests
{
    private static ISessionFactory _sessionFactory;

    private NestedSetRepository _repository;
    private int _rootId;
    private FolderController _folderController;

    private List<FolderItem> _expected = new List<FolderItem>()
    {
        new FolderItem() { Id = 2, Name = "Ordner 1", Children = new List<FolderItem>()
            {
                new FolderItem() { Id = 3, Name = "Ordner 1.1" },
                new FolderItem() { Id = 4, Name = "Ordner 1.2", Children = new List<FolderItem>()
                    {
                        new FolderItem() { Id = 5, Name = "Ordner 1.2.1"},
                        new FolderItem() { Id = 6, Name = "Ordner 1.2.2"},
                        new FolderItem() { Id = 7, Name = "Ordner 1.2.3"},
                    }
                },
                new FolderItem() { Id = 8, Name = "Ordner 1.3"},
            },
        },
        new FolderItem() { Id = 9, Name = "Ordner 2", Children = new List<FolderItem>()
            {
                new FolderItem() { Id = 10, Name = "Ordner 2.1"},
                new FolderItem() { Id = 11, Name = "Ordner 2.2"},
            },
        }
    };


    [SetUp]
    public void SetUp()
    {
        var cfg = new Configuration().Configure("ModuleA.Server.Controllers.Tests.dll.config");

        var schemaCreate = new NHibernate.Tool.hbm2ddl.SchemaExport(cfg);
        schemaCreate.Create(false, true);

        _sessionFactory = cfg.BuildSessionFactory();

        _repository = new NestedSetRepository(_sessionFactory.OpenSession());
        Database.Database.CreateInitialData(_repository);
        _folderController = new FolderController(_repository);
    }

    [TearDown]
    public void Cleanup()
    {
        _sessionFactory.Close();
        _repository.Dispose();
    }


    [Test]
    public void GetAllFolder_Returns_AllFolder()
    {
        var result1 = _folderController.GetAll();
        result1.Result.ShouldBeOfType<OkObjectResult>();
        var jsonResult1 = JsonConvert.SerializeObject(((OkObjectResult)result1.Result).Value);
        jsonResult1.ShouldBeEquivalentTo(JsonConvert.SerializeObject(_expected));
    }


    [Test]
    public void MoveFolder_Returns_OK()
    {
        var expected2 = new List<FolderItem>()
        {
            new FolderItem() { Id = 2, Name = "Ordner 1", Children = new List<FolderItem>()
                {
                    new FolderItem() { Id = 3, Name = "Ordner 1.1", Children = new List<FolderItem>()
                        {
                            new FolderItem() { Id = 9, Name = "Ordner 2", Children = new List<FolderItem>()
                                {
                                    new FolderItem() { Id = 10, Name = "Ordner 2.1"},
                                    new FolderItem() { Id = 11, Name = "Ordner 2.2"},
                                },
                            }
                        }
                    },
                    new FolderItem() { Id = 4, Name = "Ordner 1.2", Children = new List<FolderItem>()
                        {
                            new FolderItem() { Id = 5, Name = "Ordner 1.2.1"},
                            new FolderItem() { Id = 6, Name = "Ordner 1.2.2"},
                            new FolderItem() { Id = 7, Name = "Ordner 1.2.3"},
                        }
                    },
                    new FolderItem() { Id = 8, Name = "Ordner 1.3"},
                },
            },
        };
        _folderController.MoveFolder(9, 3);
        var result2 = _folderController.GetAll();
        result2.Result.ShouldBeOfType<OkObjectResult>();
        var jsonResult2 = JsonConvert.SerializeObject(((OkObjectResult)result2.Result).Value);
        jsonResult2.ShouldBeEquivalentTo(JsonConvert.SerializeObject(expected2));
    }

    [Test]
    public void MoveFolder_Returns_NotFound()
    {
        var result1 = _folderController.MoveFolder(99, 5);
        result1.ShouldBeOfType<NotFoundResult>();
    }

    [Test]
    public void MoveFolder_Returns_NotFoundParent()
    {
        var result1 = _folderController.MoveFolder(4, 99);
        result1.ShouldBeOfType<NotFoundResult>();
    }

    [Test]
    public void MoveFolder_Returns_BadRequest()
    {
        var result1 = _folderController.MoveFolder(4, 5);
        result1.ShouldBeOfType<BadRequestResult>();
    }
}
