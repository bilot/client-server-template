using Microsoft.AspNetCore.Mvc;
using NHibernate.Cfg;
using NHibernate;
using ModuleA.Server.Database.Models;
using ModuleA.Server.Database.Repository;
using ModuleA.Contracts.Models;
using ModuleA.Server.Integration;
using Newtonsoft.Json;

namespace ModuleA.Server.Controllers.Tests;

[TestFixture]
public class FolderControllerTests
{
    private static ISessionFactory _sessionFactory;

    private NestedSetRepository _repository;
    private int _rootId;
    private FolderController _folderController;

    [SetUp]
    public void SetUp()
    {
        var cfg = new Configuration().Configure("ModuleA.Server.Controllers.Tests.dll.config");

        var schemaCreate = new NHibernate.Tool.hbm2ddl.SchemaExport(cfg);
        schemaCreate.Create(false, true);

        _sessionFactory = cfg.BuildSessionFactory();

        _repository = new NestedSetRepository(_sessionFactory.OpenSession());
        _rootId = _repository.CreateRoot(new NestedSetNode() { Name = "Root" }).Id;
        _folderController = new FolderController(_repository);
    }

    [TearDown]
    public void Cleanup()
    {
        _sessionFactory.Close();
        _repository.Dispose();
    }


    [Test]
    public void GetAllFolder_Returns_AllFolder()
    {
        var expected = new List<FolderItem> {
            new FolderItem { Id = 2, Name = "Ordner 1" }
        };

        _repository.InsertAsLastChildOf(_repository.GetById(_rootId), new NestedSetNode() { Name = "Ordner 1" });
        var result = _folderController.GetAll();
        result.Result.ShouldBeOfType<OkObjectResult>();
        var returnedItems = ((OkObjectResult)result.Result).Value;
        returnedItems.ShouldBeEquivalentTo(expected);
    }

    [Test]
    public void AddFolder_Returns_OK()
    {
        var expected1 = new FolderItem { Id = 2, Name = "Neuer Ordner" };
        var expected2 = new FolderItem { Id = 3, Name = "Neuer Ordner 1" };
        var expected3 = new List<FolderItem> {
            expected1,
            expected2
        };

        var result1 = _folderController.AddFolder(_rootId);
        result1.Result.ShouldBeOfType<OkObjectResult>();
        var returnedObject1 = ((OkObjectResult)result1.Result).Value;
        returnedObject1.ShouldBeOfType<FolderItem>();
        returnedObject1.ShouldBeEquivalentTo(expected1);

        var result2 = _folderController.AddFolder(_rootId);
        result2.Result.ShouldBeOfType<OkObjectResult>();
        var returnedObject2 = ((OkObjectResult)result2.Result).Value;
        returnedObject2.ShouldBeOfType<FolderItem>();
        returnedObject2.ShouldBeEquivalentTo(expected2);

        var result3 = _folderController.GetAll();
        result3.Result.ShouldBeOfType<OkObjectResult>();
        var returnedItems3 = ((OkObjectResult)result3.Result).Value;
        returnedItems3.ShouldBeEquivalentTo(expected3);
    }

    [Test]
    public void RenameFolder_Returns_OK()
    {
        var expected1 = new FolderItem { Id = 2, Name = "Neuer Ordner" };
        var expected3 = new List<FolderItem> {
            new FolderItem { Id = 2, Name = "Rename Name" }
        };

        var result1 = _folderController.AddFolder(_rootId);
        result1.Result.ShouldBeOfType<OkObjectResult>();
        var returnedObject1 = ((OkObjectResult)result1.Result).Value as FolderItem;
        returnedObject1.ShouldBeEquivalentTo(expected1);

        var result2 = _folderController.RenameFolder(returnedObject1.Id, "Rename Name");
        result2.ShouldBeOfType<OkResult>();

        var result3 = _folderController.GetAll();
        result3.Result.ShouldBeOfType<OkObjectResult>();
        var returnedItems3 = ((OkObjectResult)result3.Result).Value;
        returnedItems3.ShouldBeEquivalentTo(expected3);
    }

    [Test]
    public void DeleteFolder_Returns_OK()
    {
        var expected1 = new FolderItem { Id = 2, Name = "Neuer Ordner" };
        var expected3 = new List<FolderItem>
        {
        };

        var result1 = _folderController.AddFolder(_rootId);
        result1.Result.ShouldBeOfType<OkObjectResult>();
        var returnedObject1 = ((OkObjectResult)result1.Result).Value as FolderItem;
        returnedObject1.ShouldBeEquivalentTo(expected1);

        var result2 = _folderController.DeleteFolder(returnedObject1.Id);
        result2.ShouldBeOfType<OkResult>();

        var result3 = _folderController.GetAll();
        result3.Result.ShouldBeOfType<OkObjectResult>();
        var returnedItems3 = ((OkObjectResult)result3.Result).Value;
        returnedItems3.ShouldBeEquivalentTo(expected3);
    }

    [Test]
    public void AddFolder_Returns_NotFound()
    {
        var result1 = _folderController.AddFolder(7);
        result1.Result.ShouldBeOfType<NotFoundResult>();
    }

    [Test]
    public void RenameFolder_Returns_NotFound()
    {
        var expected1 = new FolderItem { Id = 2, Name = "Neuer Ordner" };
        var expected3 = new List<FolderItem> {
            new FolderItem { Id = 2, Name = "Rename Name" }
        };

        var result1 = _folderController.AddFolder(_rootId);
        result1.Result.ShouldBeOfType<OkObjectResult>();
        var returnedObject1 = ((OkObjectResult)result1.Result).Value as FolderItem;
        returnedObject1.ShouldBeEquivalentTo(expected1);

        var result2 = _folderController.RenameFolder(7, "Rename Name");
        result2.ShouldBeOfType<NotFoundResult>();
    }

    [Test]
    public void DeleteFolder_Returns_NotFound()
    {
        var result1 = _folderController.DeleteFolder(7);
        result1.ShouldBeOfType<NotFoundResult>();
    }

    [Test]
    public void DeleteFolder_Returns_BadRequest()
    {
        var result1 = _folderController.DeleteFolder(1);
        result1.ShouldBeOfType<BadRequestResult>();
    }
}
