﻿using MvvmGen;
using System.Collections.ObjectModel;

namespace ModuleA.Client.Models;

[ViewModelGenerateInterface]
[ViewModel]
public partial class FolderItem
{
    [Property] private int _id;
    [Property] private string _name;
    [Property] private bool _isExpanded;
    [Property] private bool _isSelected;
    [Property] private bool _isEdited;
    [Property] private ObservableCollection<FolderItem> _children;
}
