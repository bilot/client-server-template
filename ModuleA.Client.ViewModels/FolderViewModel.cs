﻿using MvvmGen;
using MvvmGen.ViewModels;
using MvvmGen.Events;
using System.Collections.ObjectModel;
using Unity;
using ModuleA.Client.Services;
using ModuleA.Client.Models;
using ModuleA.Client.Events;
using FolderItemDTO = ModuleA.Contracts.Models.FolderItem;

namespace ModuleA.Client.ViewModels;


[ViewModel]
[Inject(typeof(APIService))]
[Inject(typeof(IEventAggregator))]
public partial class FolderViewModel : ViewModelBase
{
    public static List<int> SelectedFolderIds = new();
    public static List<int> ExpandedFolderIds = new();

    [Property]
    private ObservableCollection<FolderItem> _folderItems;
    
    private FolderItem? _editFolder = null;

    partial void OnInitialize()
    {
        FolderItems = new ObservableCollection<FolderItem>();
        LoadAsync();
    }

    private async void LoadAsync()
    {
        FolderItems.Clear();

        AddRekursiv(FolderItems, await APIService.GetAllFolder());
    }

    private void SaveState()
    {
        SelectedFolderIds.Clear();
        ExpandedFolderIds.Clear();

        SaveStateRekursiv(FolderItems);
    }


    private int GetNextId(ObservableCollection<FolderItem> folderItems, int id)
    {
        for (int i = 0; i < folderItems.Count; i++)
        {
            if (folderItems[i].Id == id)
            {
                if (i < folderItems.Count - 1)
                    return folderItems[i + 1].Id;
                else if (i > 0)
                    return folderItems[i - 1].Id;
            }

            if (folderItems[i].Children.Count == 1 && folderItems[i].Children[0].Id == id)
                return folderItems[i].Id;

            var nextId = GetNextId(folderItems[i].Children, id);
            if (nextId > 0)
                return nextId;
        }

        return -1;
    }


    private static void SaveStateRekursiv(ObservableCollection<FolderItem> folderItems)
    {
        foreach (var folderItem in folderItems)
        {
            if (folderItem.IsSelected)
                SelectedFolderIds.Add(folderItem.Id);
            if (folderItem.IsExpanded)
            {
                ExpandedFolderIds.Add(folderItem.Id);
                SaveStateRekursiv(folderItem.Children);
            }
        }
    }

    private static void AddRekursiv(ObservableCollection<FolderItem> folderItems, ICollection<FolderItemDTO> folderItemDtos)
    {
        foreach (var folderItemDto in folderItemDtos)
        {
            var folderItem = new FolderItem();
            folderItem.Id = folderItemDto.Id;
            folderItem.Name = folderItemDto.Name;
            folderItem.IsSelected = SelectedFolderIds.Contains(folderItem.Id);
            folderItem.IsExpanded = ExpandedFolderIds.Contains(folderItem.Id);
            folderItem.Children = new ObservableCollection<FolderItem>();
            AddRekursiv(folderItem.Children, folderItemDto.Children);

            folderItems.Add(folderItem);
        }
    }

    [Command]
    private async void SelectFolder(object parameter)
    {
        if (parameter is FolderItem)
        {
            var folderItem = parameter as FolderItem;
            if (folderItem != null)
            {
                EventAggregator.Publish(new SelectFolderEvent(folderItem.Id));

                // ToDo EventAggregator.RegisterSubscriber ...
            }
        }
    }

    [Command]
    private async void MoveFolder(object parameter)
    {
        var values = (object[])parameter;
        var sourceItem = values[0];
        var targetItem = values[1];

        var targetId = 1;
        if (targetItem != null && targetItem is FolderItem)
        {
            var targetFolderItem = targetItem as FolderItem;
            if (targetFolderItem != null)
            {
                targetFolderItem.IsExpanded = true;
                targetId = targetFolderItem.Id;
            }
        }

        if (sourceItem is FolderItem)
        {
            var sourceFolderItem = sourceItem as FolderItem;
            if (sourceFolderItem != null)
            {
                await APIService.MoveFolder(sourceFolderItem.Id, targetId);
                SaveState();
                LoadAsync();
                EventAggregator.Publish(new SetFocusToTree());
            }
        }
    }

    [Command]
    private async void ResetEditedFolder()
    {
        if (_editFolder != null && _editFolder.IsEdited)
        {
            _editFolder.IsEdited = false;
            SaveState();
            LoadAsync();
            EventAggregator.Publish(new SetFocusToTree());
        }
    }

    [Command]
    private async void RefreshFolder()
    {
        SaveState();
        LoadAsync();
        EventAggregator.Publish(new SetFocusToTree());
    }

    [Command]
    private async void EditFolder(object parameter)
    {
        if (parameter is FolderItem)
        {
            _editFolder = parameter as FolderItem;
            if (_editFolder != null && !_editFolder.IsEdited)
            {
                _editFolder.IsEdited = true;
            }
        }
    }

    [Command]
    private async void RenameFolder(object parameter)
    {
        if (_editFolder != null && _editFolder.IsEdited)
        {
            _editFolder.IsEdited = false;
            await APIService.RenameFolder(_editFolder.Id, _editFolder.Name);
            SaveState();
            LoadAsync();
            EventAggregator.Publish(new SetFocusToTree());
        }
    }


    [Command]
    private async void AddFolder(object parameter)
    {
        int parentId = 1;
        if (parameter is FolderItem)
        {
            var folderItem = parameter as FolderItem;
            if (folderItem != null)
            {
                parentId = folderItem.Id;
                folderItem.IsExpanded = true;
            }
        }

        await APIService.AddFolder(parentId);
        SaveState();
        LoadAsync();
        EventAggregator.Publish(new SetFocusToTree());
    }

    [Command]
    private async void DeleteFolder(object parameter)
    {
        if (parameter is FolderItem)
        {
            var folderItem = parameter as FolderItem;
            if (folderItem != null)
            {

                var nextFolderId = GetNextId(_folderItems, folderItem.Id);
                await APIService.DeleteFolder(folderItem.Id);
                SaveState();
                SelectedFolderIds.Clear();
                SelectedFolderIds.Add(nextFolderId);
                LoadAsync();
                EventAggregator.Publish(new SetFocusToTree());
            }
        }
    }
}