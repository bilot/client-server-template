﻿namespace ModuleA.Server.Database.Repository;

public interface IRepository<T>
{
    int Add(T entity);
    void Delete(T entity);
    void Update(T entity);
    T GetById(int id);
    List<T> GetAll();
}