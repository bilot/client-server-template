﻿using NHibernate;
using NHibernate.Linq;
using ModuleA.Server.Database.Models;

namespace ModuleA.Server.Database.Repository;

public class NestedSetRepository : Repository<NestedSetNode>
{
    public NestedSetRepository() : base()
    {
    }

    public NestedSetRepository(ISession session) : base(session)
    {
    }
 
    private void ModifyTree(int left, int right, int width)
    {
        _session.Query<NestedSetNode>()
            .Where(x => x.Left >= left)
            .Update(x => new { Left = x.Left + width });
        _session.Query<NestedSetNode>()
            .Where(x => x.Right >= right)
            .Update(x => new { Right = x.Right + width });
        _session.Clear();
    }

    private void ModifySubTree(int left, int right, int offset)
    {
        _session.Query<NestedSetNode>()
            .Where(x => x.Left >= left && x.Right <= right)
            .Update(x => new { Left = x.Left + offset, Right = x.Right + offset });
        _session.Clear();
    }

    public NestedSetNode CreateRoot(NestedSetNode insertNode)
    {
        _session.Query<NestedSetNode>().Delete();

        insertNode.Left = 0;
        insertNode.Right = 1;
        insertNode.Level = 0;
        _session.Save(insertNode);

        return insertNode;
    }

    public NestedSetNode? GetRootNode()
    {
        return _session.Query<NestedSetNode>()
            .Where(x => x.Left == 0)
            .SingleOrDefault();
    }


    public override List<NestedSetNode> GetAll()
    { 
        return _session.Query<NestedSetNode>().OrderBy(x => x.Left).ToList();
    }

    public override void Delete(NestedSetNode node)
    {
        _session.Query<NestedSetNode>()
            .Where(x => x.Left >= node.Left && x.Right <= node.Right)
            .Delete();
        ModifyTree(node.Left, node.Right, -(node.Right - node.Left + 1));
    }

    public NestedSetNode InsertAsFirstChildOf(NestedSetNode parentNode, NestedSetNode insertNode)
    {
        insertNode.Level = parentNode.Level + 1;
        insertNode.Left = parentNode.Left + 1;
        insertNode.Right = insertNode.Left + 1;

        ModifyTree(parentNode.Left + 1, parentNode.Left + 1, 2);
        _session.Save(insertNode);

        return insertNode;
    }

    public NestedSetNode InsertAsLastChildOf(NestedSetNode parentNode, NestedSetNode insertNode)
    {
        insertNode.Level = parentNode.Level + 1;
        insertNode.Left = parentNode.Right;
        insertNode.Right = insertNode.Left + 1;

        ModifyTree(parentNode.Right, parentNode.Right, 2);
        _session.Save(insertNode);

        return insertNode;
    }

    public NestedSetNode InsertAsPrevSiblingOf(NestedSetNode siblingNode, NestedSetNode insertNode)
    {
        insertNode.Level = siblingNode.Level;
        insertNode.Left = siblingNode.Left;
        insertNode.Right = siblingNode.Right;

        ModifyTree(siblingNode.Left, siblingNode.Left, 2);
        _session.Save(insertNode);

        return insertNode;
    }

    public NestedSetNode InsertAsNextSiblingOf(NestedSetNode siblingNode, NestedSetNode insertNode)
    {
        insertNode.Level = siblingNode.Level;
        insertNode.Left = siblingNode.Right + 1;
        insertNode.Right = insertNode.Left + 1;

        ModifyTree(siblingNode.Right + 1, siblingNode.Right + 1, 2);
        ModifyTree(siblingNode.Right + 1, siblingNode.Right + 1, 2);
        _session.Save(insertNode);

        return insertNode;
    }

    public void MoveTo(NestedSetNode node, int newPos, int newLevel)
    {
        // calculate position adjustment variables
        int width = node.Right - node.Left + 1;
        int offset = newPos - node.Left;
        int tmpLeft = node.Left;
        int tmpRight = node.Right;

        // backwards movement must account for new space
        if (offset < 0)
        {
            offset -= width;
            tmpLeft += width;
            tmpRight += width;
        }

        //*--update Level subtree
        _session.Query<NestedSetNode>()
            .Where(x => x.Left >= node.Left && x.Right <= node.Right)
            .Update(x => new { Level = x.Level + newLevel - node.Level });
        _session.Clear();

        //*--create new space for subtree
        ModifyTree(newPos, newPos, width);

        //*--move subtree into new space
        ModifySubTree(tmpLeft, tmpRight, offset);

        //*--remove old space vacated by subtree
        ModifyTree(tmpRight, tmpRight, -width);
    }

    public string GetUniqueName(NestedSetNode parent, string name)
    {
        var originalName = name;
        for (int n = 0; ; n++)
        {
            if (n > 0)
                originalName = $"{name} {n}";

            if (_session.Query<NestedSetNode>()
                .Where(x => x.Left > parent.Left && x.Right < parent.Right && x.Level == parent.Level + 1 && x.Name == originalName)
                .FirstOrDefault() == null)
                break;
        }
        return originalName;
    }
}
