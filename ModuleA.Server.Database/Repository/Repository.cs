﻿using NHibernate;

namespace ModuleA.Server.Database.Repository;

public class Repository<T> : IRepository<T>, IDisposable
{
    protected ISession _session = null;
    protected ITransaction _transaction = null;

    public Repository()
    {
        _session = Database.OpenSession();
    }

    public Repository(ISession session)
    {
        _session = session;
    }

    #region Transaction and Session Management Methods

    public void BeginTransaction()
    {
        _transaction = _session.BeginTransaction();
    }

    public void CommitTransaction()
    {
        // _transaction will be replaced with a new transaction            // by NHibernate, but we will close to keep a consistent state.
        _transaction.Commit();

        CloseTransaction();
    }

    public void RollbackTransaction()
    {
        // _session must be closed and disposed after a transaction            // rollback to keep a consistent state.
        _transaction.Rollback();

        CloseTransaction();
        CloseSession();
    }

    private void CloseTransaction()
    {
        _transaction.Dispose();
        _transaction = null;
    }

    private void CloseSession()
    {
        _session.Close();
        _session.Dispose();
        _session = null;
    }

    #endregion

    #region IRepository Members
    public virtual int Add(T entity)
    {
        int newId = (int)_session.Save(entity);
        _session.Flush();

        return newId;
    }

    public virtual void Delete(T entity)
    { 
        _session.Delete(entity); 
    }

    public virtual void Update(T entity)
    { 
        _session.Update(entity);
        _session.Flush();
    }
    
    public virtual T GetById(int id)
    {
        return _session.Get<T>(id);
    }

    public virtual List<T> GetAll()
    {
        return _session.Query<T>().ToList();
    }

    #endregion

    #region IDisposable Members

    public void Dispose()
    {
        if (_transaction != null)
        {
            // Commit transaction by default, unless user explicitly rolls it back.
            // To rollback transaction by default, unless user explicitly commits,                // comment out the line below.
            CommitTransaction();
        }

        if (_session != null)
        {
            _session.Flush(); // commit session transactions
            CloseSession();
        }
    }

    #endregion
}
