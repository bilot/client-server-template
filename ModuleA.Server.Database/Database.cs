﻿using NHibernate;
using NHibernate.Cfg;
using ModuleA.Server.Database.Models;
using ModuleA.Server.Database.Repository;

namespace ModuleA.Server.Database;

public static class Database
{
    private static ISessionFactory _sessionFactory;

    private static ISessionFactory SessionFactory
    {
        get
        {
            if (_sessionFactory == null)
            {
                var cfg = new Configuration()
                    .Configure();

                SchemaUpdate(cfg);

                _sessionFactory = cfg.BuildSessionFactory();

                var repository = new NestedSetRepository();
                if (repository.GetAll().ToList().Count() == 0)
                {
                    CreateInitialData(repository);
                }
            }

            return _sessionFactory;
        }
    }

    private static void SchemaUpdate(Configuration cfg)
    {
        var schemaUpdate = new NHibernate.Tool.hbm2ddl.SchemaUpdate(cfg);
        schemaUpdate.Execute(false, true);
    }


    public static void CreateInitialData(NestedSetRepository repository)
    {
        var rootId = repository.CreateRoot(new NestedSetNode() { Name = "Root" }).Id;
        var parentId1 = repository.InsertAsLastChildOf(repository.GetById(rootId), new NestedSetNode() { Name = "Ordner 1" }).Id;
        repository.InsertAsLastChildOf(repository.GetById(parentId1), new NestedSetNode() { Name = "Ordner 1.1" });
        var parentId2 = repository.InsertAsLastChildOf(repository.GetById(parentId1), new NestedSetNode() { Name = "Ordner 1.2" }).Id;
        repository.InsertAsLastChildOf(repository.GetById(parentId2), new NestedSetNode() { Name = "Ordner 1.2.1" });
        repository.InsertAsLastChildOf(repository.GetById(parentId2), new NestedSetNode() { Name = "Ordner 1.2.2" });
        repository.InsertAsLastChildOf(repository.GetById(parentId2), new NestedSetNode() { Name = "Ordner 1.2.3" });
        repository.InsertAsLastChildOf(repository.GetById(parentId1), new NestedSetNode() { Name = "Ordner 1.3" });
        var parentId3 = repository.InsertAsLastChildOf(repository.GetById(rootId), new NestedSetNode() { Name = "Ordner 2" }).Id;
        repository.InsertAsLastChildOf(repository.GetById(parentId3), new NestedSetNode() { Name = "Ordner 2.1" });
        repository.InsertAsLastChildOf(repository.GetById(parentId3), new NestedSetNode() { Name = "Ordner 2.2" });
    }

    public static ISession OpenSession()
    {
        return SessionFactory.OpenSession();
    }
}