﻿namespace ModuleA.Server.Database.Models;

public partial class NestedSetNode
{
    public virtual int Id { get; set; }

    public virtual string Name { get; set; } = null!;

    public virtual int Left { get; set; }

    public virtual int Right { get; set; }

    public virtual int Level { get; set; }
}
