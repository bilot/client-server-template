﻿using CommonServiceLocator;
using log4net;
using MvvmGen.Events;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ModuleA.Client.Events;
using ModuleA.Client.Models;
using ModuleA.Client.ViewModels;

namespace ModuleA.Client.Views;

public partial class FolderView : UserControl, IEventSubscriber<SetFocusToTree>
{
    private static readonly ILog Logger = LogManager.GetLogger(typeof(FolderView));
    private Point _lastMouseDown; // ToDo
    private FolderItem? _draggedItem;
    private FolderItem? _target;

    public FolderView()
    {
        var eventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();
        eventAggregator.RegisterSubscriber(this);

        InitializeComponent();

        _treeView.MouseLeftButtonDown += TreeView_MouseLeftButtonDown;
    }

    public void OnEvent(SetFocusToTree eventData)
    {
        _treeView.Focus();
    }

    
    private void TreeView_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
    {
        if (sender is not TreeViewItem)
        {
            var item = _treeView.SelectedItem as FolderItem;
            if (item != null)
            {
                _treeView.Focus();
                item.IsSelected = false;
            }
        }

        e.Handled = true;
    }

    private void TreeView_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
    {
        if (sender is TreeViewItem)
        {
            ((TreeViewItem)sender).IsSelected = true;
        }
        e.Handled = true;
    }

    private void TreeView_MouseMove(object sender, MouseEventArgs e)
    {
        try
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                UIElement element = e.OriginalSource as UIElement;
                Point currentPosition = e.GetPosition(_treeView);
                if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                    (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                {
                    _draggedItem = (FolderItem)_treeView.SelectedItem;
                    if (_draggedItem != null)
                    {
                        DragDropEffects finalDropEffect = DragDrop.DoDragDrop(_treeView, new DataObject("FolderItem", _treeView.SelectedValue), DragDropEffects.Move);
                        if (finalDropEffect == DragDropEffects.Move)
                        {
                            // A Move drop was accepted
                            if (_target == null || _draggedItem.Id != _target.Id)
                            {
                                MoveItem(_draggedItem, _target);
                                _target = null;
                                _draggedItem = null;
                            }
                        }
                    }
                }
            }
        }
        catch (Exception exception)
        {
            Logger.Error(exception);
        }
    }

    private void MoveItem(FolderItem draggedItem, FolderItem? target)
    {
        if (DataContext is FolderViewModel viewModel)
        {
            viewModel.MoveFolderCommand.Execute(new object[] { draggedItem, target });
        }
    }

    private void TreeView_DragOver(object sender, DragEventArgs e)
    {
        try
        {
            Point currentPosition = e.GetPosition(_treeView);
            if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
            {
                // Verify that this is a valid drop and then store the drop target
                FolderItem item = GetNearestContainer(e.OriginalSource as UIElement);
                if (_draggedItem?.Id != item.Id)
                {
                    e.Effects = DragDropEffects.Move;
                }
                else
                {
                    e.Effects = DragDropEffects.None;
                }
            }
            e.Handled = true;
        }
        catch (Exception exception)
        {
            Logger.Error(exception);
        }
    }

    private void TreeView_Drop(object sender, DragEventArgs e)
    {
        try
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;

            // Verify that this is a valid drop and then store the drop target
            FolderItem TargetItem = GetNearestContainer(e.OriginalSource as UIElement);
            if (TargetItem != null && _draggedItem != null)
            {
                _target = TargetItem;
                e.Effects = DragDropEffects.Move;
            }
        }
        catch (Exception exception)
        {
            Logger.Error(exception);
        }
    }

    private FolderItem GetNearestContainer(UIElement element)
    {
        // Walk up the element tree to the nearest tree view item.
        TreeViewItem UIContainer = FindParent<TreeViewItem>(element);
        FolderItem NVContainer = null;

        if (UIContainer != null)
        {
            NVContainer = UIContainer.DataContext as FolderItem;
        }
        return NVContainer;
    }

    private static Parent FindParent<Parent>(DependencyObject child)
            where Parent : DependencyObject
    {
        DependencyObject parentObject = child;
        parentObject = VisualTreeHelper.GetParent(parentObject);

        //check if the parent matches the type we're looking for
        if (parentObject is Parent || parentObject == null)
        {
            return parentObject as Parent;
        }
        else
        {
            //use recursion to proceed with next level
            return FindParent<Parent>(parentObject);
        }
    }
}