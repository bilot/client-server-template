using ModuleA.Server.Database.Models;
using Newtonsoft.Json;


namespace ModuleA.Server.Integration.Tests;

public class FolderIntegrationTests
{
    [Test]
    public void TestException()
    {
        var nestedSetNodes = new List<NestedSetNode>();
        Should.Throw<Exception>(() => FolderIntegration.TransformNestedSetToDtosHierarchy(nestedSetNodes));
    }

    [TestCase(
        "[{\"Id\":1,\"Name\":\"Root\",\"Left\":0,\"Right\":1,\"Level\":0}]",
        "[]"
        )]
    [TestCase(
        "[{\"Id\":1,\"Name\":\"Root\",\"Left\":0,\"Right\":3,\"Level\":0},{\"Id\":2,\"Name\":\"Ordner 1\",\"Left\":1,\"Right\":2,\"Level\":1}]",
        "[{\"Id\":2,\"Name\":\"Ordner 1\",\"Children\":[]}]"
        )]
    [TestCase(
        "[{\"Id\":1,\"Name\":\"Root\",\"Left\":0,\"Right\":17,\"Level\":0},{\"Id\":2,\"Name\":\"Ordner 1\",\"Left\":1,\"Right\":14,\"Level\":1},{\"Id\":3,\"Name\":\"Ordner 1.1\",\"Left\":2,\"Right\":3,\"Level\":2},{\"Id\":4,\"Name\":\"Ordner 1.2\",\"Left\":4,\"Right\":11,\"Level\":2},{\"Id\":5,\"Name\":\"Ordner 1.2.1\",\"Left\":5,\"Right\":6,\"Level\":3},{\"Id\":6,\"Name\":\"Ordner 1.2.2\",\"Left\":7,\"Right\":8,\"Level\":3},{\"Id\":7,\"Name\":\"Ordner 1.2.3\",\"Left\":9,\"Right\":10,\"Level\":3},{\"Id\":8,\"Name\":\"Ordner 1.3\",\"Left\":12,\"Right\":13,\"Level\":2},{\"Id\":9,\"Name\":\"Ordner 2\",\"Left\":15,\"Right\":16,\"Level\":1}]",
        "[{\"Id\":2,\"Name\":\"Ordner 1\",\"Children\":[{\"Id\":3,\"Name\":\"Ordner 1.1\",\"Children\":[]},{\"Id\":4,\"Name\":\"Ordner 1.2\",\"Children\":[{\"Id\":5,\"Name\":\"Ordner 1.2.1\",\"Children\":[]},{\"Id\":6,\"Name\":\"Ordner 1.2.2\",\"Children\":[]},{\"Id\":7,\"Name\":\"Ordner 1.2.3\",\"Children\":[]}]},{\"Id\":8,\"Name\":\"Ordner 1.3\",\"Children\":[]}]},{\"Id\":9,\"Name\":\"Ordner 2\",\"Children\":[]}]"
        )]
    public void TestTransform(string jsonNestedSetNodes, string expected)
    {
        var nestedSetNodes = JsonConvert.DeserializeObject<List<NestedSetNode>>(jsonNestedSetNodes);
        var result = JsonConvert.SerializeObject(FolderIntegration.TransformNestedSetToDtosHierarchy(nestedSetNodes));

        result.ShouldBe(expected);
    }
}